// flip_cards.cpp

#include "flip_cards.hpp"

State::State(unsigned int number_of_cards) {

  for (unsigned int i = 0; i < number_of_cards; ++i)
    cards.push_back(1);

}

State::State(const std::vector<bool>& deck)
  :cards{deck}
{}

void State::set_turned_states(const std::vector<std::shared_ptr<State>>& states){
  turned_states = states;
  min_max = Result();
}

State::Result State::get_min_max() const{

  if(!min_max.max){
    if(turned_states.size()==0) min_max.min = 0;
    for(auto& state : turned_states){
      Result r = state->get_min_max();

      if (r.min < min_max.min) {
        min_max.min = r.min+1;
	min_max.min_turned_state = state;
      } // end if new min

      
      if (r.max >= min_max.max){
	min_max.max = r.max+1;
	min_max.max_turned_state = state;
      } // end if new max
      
    }// end for each state
  }// end if not max
  
  return min_max;
}

std::ostream& operator<<(std::ostream& o, const State& deck){
  for(const auto& c : deck.cards) o << c;

  if(deck.out == State::Output::MIN){

    if (deck.min_max.min_turned_state != nullptr) {
      deck.min_max.min_turned_state->out = deck.out;
      o << " --> " << *deck.min_max.min_turned_state;
    }

  } else if (deck.min_max.max_turned_state != nullptr) {
    deck.min_max.max_turned_state->out = deck.out;
    o << " --> " << *deck.min_max.max_turned_state;
  }
  
  return o;
}

unsigned int State::print(State::Output o) const{
  Result res = get_min_max(); 
  unsigned int result = o == Output::MIN ? res.min : res.max;
  out = o;
  std::cout << result
	    << " :: "
 	    << *this
	    << std::endl;
  return result;
}

//-------------------------------------------------------------------

std::vector<std::vector<bool>> turns_of(const std::vector<bool>& state){
  std::vector<std::vector<bool>> result;

  for(unsigned int i = 0; i< state.size(); ++i){
    if(state[i]){
      std::vector<bool> turn = state;
      turn[i] = 0;
      unsigned int j=i+1;
      if(j<turn.size()) turn[j] = !turn[j]; 

      result.push_back(turn);
    } // end if the position is a 1
  } // end for each position in state
  
  return result;
}

unsigned int set_number_of_cards(int argc, char* argv[], int number_of_cards){
  if (argc > 1){
    int parameter = std::stoul(argv[1]);
    if (parameter)
      number_of_cards = parameter;
  }

  return number_of_cards;
}

void
process(std::shared_ptr<State>& start){

  std::vector<std::shared_ptr<State>> working_states{start};
  std::map<std::vector<bool>, std::shared_ptr<State>> existing_states{ {(*start)(), start} };
  
  while( working_states.size() ){
    State* current = working_states.back().get();
    working_states.pop_back();
    std::vector<bool> current_state = (*current)();
    std::vector<std::vector<bool>> states_from_current = turns_of(current_state);
    std::vector<std::shared_ptr<State>> ptr_states_from_current;

    for(std::vector<bool>& state_from : states_from_current){

      if(existing_states.count(state_from)){
	// take the existing State as child of the current processed state
	ptr_states_from_current.push_back(existing_states.at(state_from));
      }else{
	std::shared_ptr<State> state_add{new State{state_from}}; // create a State
	existing_states.insert( {state_from,state_add} ); // mark the state as a existing state 
	ptr_states_from_current.push_back(state_add); // for adding as child_state of the current processed state
	working_states.push_back(state_add); // add the new state to the states to process
      }// end if state exists or not
      
    }// end for all states_from_current
    current->set_turned_states(ptr_states_from_current);
      
  } // end while there are states to process

}
