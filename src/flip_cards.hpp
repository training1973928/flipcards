// flip_cards.hpp

#ifndef FLIP_CARDS_HPP
#define FLIP_CARDS_HPP FLIP_CARDS_HPP

#include <iostream>
#include <vector>
#include <memory>
#include <map>

class State {
public:
  State(unsigned int number_of_cards);
  State(const std::vector<bool>& deck);
  void set_turned_states(const std::vector<std::shared_ptr<State>>& states);
  struct Result{
    std::shared_ptr<State> min_turned_state = nullptr;
    std::shared_ptr<State> max_turned_state = nullptr;
    unsigned int min = -1;
    unsigned int max =  0;
  };
  std::vector<bool> operator()(){return cards;}
  mutable enum class Output{MIN,MAX} out = Output::MIN;
  Result get_min_max() const;
  friend
  std::ostream& operator<< (std::ostream& o, const State& deck); 
  unsigned int print(Output o = Output::MIN) const;
private:
  std::vector<bool> cards;
  std::vector<std::shared_ptr<State>> turned_states;
  mutable Result min_max;
};

//_______________________________________________________________________
/* turns_of
   returns all possible turns to a given state of a deck
   where a 1 turns to 0, and the right neighbour flips,
   if there is a right neighbour
 */
std::vector<std::vector<bool>> turns_of(const std::vector<bool>& state);

/* set_number_of_cards
   if the program is given a numeric parameter,
   take this as number of cards
   else use the default value <20> for that 
 */
unsigned int set_number_of_cards(int argc, char* argv[], int number_of_cards=20);

/* process
   creates a graph from start with all possible turns 
   of a card deck with
   the a vector of bool, where 1 is a hidden an 0 a unhidden card.
   Needed is the pointer to the starting State Element, from which
   we have to find all posibilities of turns until all cards
   are unhidden.
 */
void  process(std::shared_ptr<State>& start);

#endif //FLIP_CARDS_HPP
