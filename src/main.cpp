//main.cpp  ::  flip_cards
/* Problembeschreibung
   Wenn man X-beliebig viele Karten mit dem Bild nach unten in eine Reihe legt,
   von denen sobald man eine umdreht immer die Karte rechts daneben ebenfalls gedreht wird,
   so werden egal welche Karte man mit Bild nach unten wählt irgendwann
   alle Karten mit Bild nach oben in der Reihe liegen.

   Das gilt es für eine gewählte Anzahl an Karten zu zeigen, wobei wir wissen wollen,
   wieviel Karten wir minimal und maximal drehen können, bis alle Karten aufgedeckt sind.
*/

#include "flip_cards.hpp"

int main(int argc, char* argv[]){

  unsigned int number_of_cards = set_number_of_cards(argc, argv); 
  std::shared_ptr<State> start{new State(number_of_cards)}; 

  process(start);

  start->print();
  start->print(State::Output::MAX);
  
  return 0;
}
